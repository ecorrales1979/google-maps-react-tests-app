// Custom messages

module.exports = {
  empty: {
    lat: 'A latitude é requerida',
    lng: 'A longitude é requerida',
    text: 'O texto do marcador é requerido'
  },
  error: {
    lat: 'A latitude deve ser um número entre -90 e 90',
    lng: 'A longitude deve ser um número entre -180 e 180',
    nan: 'Por favor, entre um número'
  }
}
