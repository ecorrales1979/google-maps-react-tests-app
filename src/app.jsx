import React from 'react'
import { hot } from 'react-hot-loader'

import Title from 'Components/header/title'
import InputMarker from 'Components/form/inputMarker'

const App = () => {
  return (
    <>
      <Title />
      <InputMarker />
    </>
  )
}

export default hot(module)(App)
