import React from 'react'
import { hot } from 'react-hot-loader'
import GoogleMapReact from 'google-map-react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarker } from '@fortawesome/free-solid-svg-icons'

const defaultProps = {
  center: {
    lat: -29.6839,
    lng: -51.4579
  },
  zoom: 10
}

export const MarkerContainer = ({ text }) => {
  return (
    <div className='marker-container'>
      <FontAwesomeIcon icon={faMapMarker} />
      <span className='marker'>{text}</span>
    </div>
  )
}

const Mapa = (props) => {
  const displayMarkers = (props) => {
    console.log(props)
    return props.markers
      ? props.markers.map((marker, idx) => {
        return (
          <MarkerContainer
            key={`marker_${idx}`}
            lat={marker.lat}
            lng={marker.lng}
            text={marker.text}
          />
        )
      })
      : true
  }

  return (
    <div className='map-container' style={{ height: '70vh', width: '90%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: 'AIzaSyApfZ_sUrJl1Tl7AWRfiGZ9ITGvnasaddI' }}
        defaultCenter={defaultProps.center}
        defaultZoom={defaultProps.zoom}
      >
        {displayMarkers(props)}
      </GoogleMapReact>
    </div>
  )
}

export default hot(module)(Mapa)
