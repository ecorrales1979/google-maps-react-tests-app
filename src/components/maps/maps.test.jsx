import React from 'react'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import Maps, { MarkerContainer } from './maps'

Enzyme.configure({ adapter: new Adapter() })

// eslint-disable-next-line no-undef
test('Rendering map', () => {
  // Cria o componente, captura a saída renderizada e cria um snapshot
  const component = renderer.create(
    <Maps />
  )
  const tree = component.toJSON()
  expect(tree).toMatchSnapshot() // eslint-disable-line no-undef
})

// eslint-disable-next-line no-undef
test('Rendering markers', () => {
  const markers = [{ lat: -29.6839, lng: -51.4579, text: 'Montenegro' }]
  // Cria o componente, captura a saída renderizada e cria um snapshot
  const wrapper = shallow(<Maps markers={markers} />)
  const component = wrapper.find(MarkerContainer)
  expect(component.html()).toMatch(/Montenegro/) // eslint-disable-line no-undef
})
