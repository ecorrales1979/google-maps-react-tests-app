import React from 'react'
import renderer from 'react-test-renderer'

import Title from './title'

// eslint-disable-next-line no-undef
test('Rendered title', () => {
  // Cria o componente, captura a saída renderizada e cria um snapshot
  const component = renderer.create(
    <Title />
  )
  const tree = component.toJSON()
  expect(tree).toMatchSnapshot() // eslint-disable-line no-undef
})
