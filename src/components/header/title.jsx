import React from 'react'
import { hot } from 'react-hot-loader'

const Title = () => {
  return <div className='title'>Google Maps React App</div>
}

export default hot(module)(Title)
