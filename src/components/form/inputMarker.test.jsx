import React from 'react'
import Enzyme, { mount, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import InputMarker from './inputMarker'

Enzyme.configure({ adapter: new Adapter() })

const simulateSubmit = wrapper => {
  const form = wrapper.find('form')
  return new Promise((resolve, reject) => {
    form.simulate('submit', {
      preventDefault: () => {}
    })
  })
}

const simulateChange = (wrapper, value) => {
  return new Promise((resolve, reject) => {
    wrapper.simulate('change', {
      persist: () => {},
      target: {
        value: value
      }
    })
    resolve()
  })
}

// eslint-disable-next-line no-undef
it('Checking if form is submitted', () => {
  const onSubmitFn = jest.fn() // eslint-disable-line no-undef
  const wrapper = mount(<InputMarker onSubmit={onSubmitFn} />)

  const form = wrapper.find('form')
  simulateSubmit(form).then(() => {
    expect(onSubmitFn).toHaveBeenCalledTimes(1) // eslint-disable-line no-undef
  })
})

// eslint-disable-next-line no-undef
it('Checking rendering of form', () => {
  const onSubmitFn = jest.fn() // eslint-disable-line no-undef
  const wrapper = mount(<InputMarker onSubmit={onSubmitFn} />)

  const label = wrapper.find('label')
  expect(label).toHaveLength(3) // eslint-disable-line no-undef

  const input = wrapper.find('input')
  expect(input).toHaveLength(3) // eslint-disable-line no-undef
})

// eslint-disable-next-line no-undef
test('Submit correct values', () => {
  // const onSubmitFn = jest.fn() // eslint-disable-line no-undef
  const wrapper = shallow(<InputMarker />)

  const lat = wrapper.find('input[name="lat"]')
  console.log('Hi')
  console.log(lat)

  simulateChange(lat).then(() => {
    simulateSubmit(wrapper).then(() => {
      const error = wrapper.find(InputMarker)
      expect(error).html().toMatch(/<div class="error error-message">A latitude deve ser um número entre -90 e 90<\/div>/) // eslint-disable-line no-undef
    })
  })
})
