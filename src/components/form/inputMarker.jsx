import React from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup'
import { Row, Col, Container, Button } from 'react-bootstrap'
import { hot } from 'react-hot-loader'

import Maps from '../maps/maps'
import messages from '../../utils/messages'

const formSchema = Yup.object().shape({
  lat: Yup.number()
    .max(90, messages.error.lat)
    .min(-90, messages.error.lat)
    .typeError(messages.error.nan)
    .required(messages.empty.lat),
  lng: Yup.number()
    .max(180, messages.error.lng)
    .min(-180, messages.error.lng)
    .typeError(messages.error.nan)
    .required(messages.empty.lng),
  text: Yup.string()
    .required(messages.empty.text)
})

const InputMarker = () => {
  const [markers, setMarkers] = React.useState([])

  const handleSubmit = e => {
    setMarkers([
      ...markers,
      {
        lat: e.lat,
        lng: e.lng,
        text: e.text
      }
    ])
  }

  const resetMarkers = () => {
    setMarkers([])
  }

  return (
    <Container fluid='true'>
      <Row>
        <Col md='6'>
          <Maps markers={markers} />
        </Col>
        <Col md='6'>
          <div><h3>Entre as coordenadas e o texto para inserir um marcador</h3></div>
          <Formik
            initialValues={{
              lat: '',
              lng: '',
              text: ''
            }}
            validationSchema={formSchema}
            onSubmit={handleSubmit}
          >
            {({ errors, touched }) => {
              return (
                <Form noValidate>
                  <div className='form-group'>
                    <label>Latitude</label>
                    <Field name='lat' className='form-control' placeholder='Latitude' />
                    <ErrorMessage name='lat'>
                      {msg => <div className='error error-message'>{msg}</div>}
                    </ErrorMessage>
                  </div>
                  <div className='form-group'>
                    <label>Longitude</label>
                    <Field name='lng' className='form-control' placeholder='Longitude' />
                    <ErrorMessage name='lng'>
                      {msg => <div className='error error-message'>{msg}</div>}
                    </ErrorMessage>
                  </div>
                  <div className='form-group'>
                    <label>Mensagem</label>
                    <Field name='text' className='form-control' placeholder='Texto do marcador' />
                    <ErrorMessage name='text'>
                      {msg => <div className='error error-message'>{msg}</div>}
                    </ErrorMessage>
                  </div>
                  <div className='d-flex justify-content-between'>
                    <Button type='submit' variant='primary'>Inserir marcador</Button>
                    <Button type='button' onClick={resetMarkers} variant='danger'>Eliminar marcadores</Button>
                  </div>
                </Form>
              )
            }}
          </Formik>
        </Col>
      </Row>
    </Container>
  )
}

export default hot(module)(InputMarker)
