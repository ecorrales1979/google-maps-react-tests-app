# Google Maps React Tests App

App para mostrar um mapa do Google Maps, com a possibilidade de adicionar um marcador com texto

## Instalação
1. Clonar o repositório para um ambiente local:
> git clone https://gitlab.com/ecorrales1979/google-maps-react-tests-app.git
2. Instalar as dependências:
> npm install
3. Iniciar o servidor:
> npm run start
4. Abrir no navegador de sua preferência o site [http://localhost:3000](http://localhost:300)
5. Usar o formulário para inserir os dados dos marcadores desejados

## Recomendações
- Se modificar o código, execute o comando:
> npm run test
- Se um teste de Snapshot não for bem sucedido e tiver certeza que o componente atual está do jeito certo, atualize os Snapshots dos testes:
> npm run test-update